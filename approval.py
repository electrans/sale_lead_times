# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.

from trytond.model import ModelView, Workflow
from trytond.pool import Pool, PoolMeta


class ApprovalRequest(metaclass=PoolMeta):
    __name__ = 'approval.request'

    @classmethod
    def __setup__(cls):
        super(ApprovalRequest, cls).__setup__()

    @classmethod
    def _get_document(cls):
        return super(ApprovalRequest, cls)._get_document() + [
            'sale.sale',
            ]

    @classmethod
    @ModelView.button
    @Workflow.transition('approved')
    def approve(cls, requests):
        pool = Pool()
        Sale = pool.get('sale.sale')
        for request in requests:
            """
            # Check if validated terms and is in time
            if isinstance(request.document, Sale) and not\
                 request.document.validated_line_terms:
                # If not terms or loose reject approval with description and\
                # raise error
                request.description = gettext(
                    'electrans_sale_lead_times.no_terms_or_loose')
                cls.cancel([request])
                request.document.lead_times_state = 'requested'
            else:
            """
            # Update sale_lead_times_state to approved
            if isinstance(request.document, Sale):
                request.document.lead_times_state = 'approved'
        super(ApprovalRequest, cls).approve(requests)

    @classmethod
    @ModelView.button
    @Workflow.transition('rejected')
    def reject(cls, requests):
        super(ApprovalRequest, cls).reject(requests)

    @classmethod
    def create(cls, vlist):
        pool = Pool()
        Sale = pool.get('sale.sale')
        sale = []
        for vals in vlist:
            document = vals.get('document')
            if isinstance(document, Sale):
                sale.append(document)
            elif (isinstance(document, str)
                    and document.startswith('sale.sale,')):
                sale.append(Sale(int(document[10:])))
        res = super(ApprovalRequest, cls).create(vlist)
        if sale:
            Sale.set_approval_state(sale)
        return res

    @classmethod
    def write(cls, *args):
        pool = Pool()
        Sale = pool.get('sale.sale')
        sales = []
        actions = iter(args)
        for records, values in zip(actions, actions):
            sales += [r.document for r in records
                if isinstance(r.document, Sale)]
            if 'document' in values:
                document = values['document']
                if isinstance(document, Sale):
                    sales.append(document)
                elif (isinstance(document, str)
                        and document.startswith('sale.sale,')):
                    sales.append(Sale(int(document[10:])))
        super(ApprovalRequest, cls).write(*args)
        if sales:
            Sale.set_approval_state(sales)

    @classmethod
    def delete(cls, requests):
        super(ApprovalRequest, cls).delete(requests)
