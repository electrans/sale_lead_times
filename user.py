from trytond.model import fields
from trytond.pool import PoolMeta


class User(metaclass=PoolMeta):
    __name__ = "res.user"

    notify_lead_times = fields.Many2One('sale.configuration', 'Notify',
                    help='Sale configuration to notify when the sale lead times is approved')
