=======================
Sale Lead Times Scenario
=======================

Imports::

    >>> import datetime
    >>> from dateutil.relativedelta import relativedelta
    >>> from decimal import Decimal
    >>> from operator import attrgetter
    >>> from proteus import config, Model, Wizard, Report
    >>> from trytond.tests.tools import activate_modules
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> from trytond.modules.account.tests.tools import create_fiscalyear, \
    ...     create_chart, get_accounts, create_tax
    >>> from trytond.modules.account_invoice.tests.tools import \
    ...     set_fiscalyear_invoice_sequences, create_payment_term


Activate modules::

    >>> config = activate_modules('electrans_sale_lead_times')

Get models::

    >>> Sale = Model.get('sale.sale')
    >>> SaleLine = Model.get('sale.line')
    >>> User = Model.get('res.user')
    >>> Group = Model.get('res.group')
    >>> Party = Model.get('party.party')
    >>> Address = Model.get('party.address')
    >>> ProductUom = Model.get('product.uom')
    >>> ProductTemplate = Model.get('product.template')
    >>> Product = Model.get('product.product')
    >>> Inventory = Model.get('stock.inventory')
    >>> Location = Model.get('stock.location')
    >>> SaleConfig = Model.get('sale.configuration')
    >>> Sequence = Model.get('ir.sequence')
    >>> SequenceType = Model.get('ir.sequence.type')
    >>> ApprovalGroup = Model.get('approval.group')
    >>> ApprovalGroupResUser = Model.get('approval.group-res.user')
    >>> ApprovalRequest = Model.get('approval.request')
    >>> today = datetime.date.today()

Create company::

    >>> _ = create_company()
    >>> company = get_company()

Create sale user::

    >>> sale_user = User()
    >>> sale_user.name = 'SaleUser'
    >>> sale_user.login = 'sale'
    >>> sale_group, = Group.find([('name', '=', 'Sales')])
    >>> lead_times_group, = Group.find([('name', '=', 'Sale lead Times')])
    >>> stock_group, = Group.find([('name', '=', 'Stock')])
    >>> to_quote_group, = Group.find([('name', '=', 'Sales to quote')])
    >>> sales_admin_group, = Group.find([('name', '=', 'Sales Administrator')])
    >>> sale_user.groups.append(sale_group)
    >>> sale_user.groups.append(lead_times_group)
    >>> sale_user.groups.append(stock_group)
    >>> sale_user.groups.append(to_quote_group)
    >>> sale_user.groups.append(sales_admin_group)
    >>> sale_user.save()


Create approval user::

    >>> approval_user = User()
    >>> approval_user.name = 'Approval Sale User'
    >>> approval_user.login = 'approval'
    >>> sale_group, = Group.find([('name', '=', 'Sales')])
    >>> lead_times_group, = Group.find([('name', '=', 'Sale lead Times')])
    >>> stock_group, = Group.find([('name', '=', 'Stock')])
    >>> to_quote_group, = Group.find([('name', '=', 'Sales to quote')])
    >>> sales_admin_group, = Group.find([('name', '=', 'Sales Administrator')])
    >>> approval_group, = Group.find([('name', '=', 'Approval')])
    >>> approval_administrator_group, = Group.find([('name', '=', 'Approval Administration')])
    >>> approval_user.groups.append(sale_group)
    >>> approval_user.groups.append(lead_times_group)
    >>> approval_user.groups.append(stock_group)
    >>> approval_user.groups.append(to_quote_group)
    >>> approval_user.groups.append(sales_admin_group)
    >>> approval_user.groups.append(approval_group)
    >>> approval_user.groups.append(approval_administrator_group)
    >>> approval_user.save()

Add user to approval group (Sale Lead Times)::

    >>> lead_times_approval, = ApprovalGroup.find(['name', '=', 'Sale Lead Times'])
    >>> approval_group_user = ApprovalGroupResUser(group=lead_times_approval,user=approval_user)
    >>> lead_times_approval.users.append(approval_user)
    >>> lead_times_approval.save()

Create fiscal year::

    >>> fiscalyear = set_fiscalyear_invoice_sequences(
    ...     create_fiscalyear(company))
    >>> fiscalyear.click('create_period')

Create chart of accounts::

    >>> _ = create_chart(company)
    >>> accounts = get_accounts(company)
    >>> revenue = accounts['revenue']
    >>> expense = accounts['expense']

Create tax::

    >>> tax = create_tax(Decimal('.10'))
    >>> tax.save()

Create a party::

    >>> party = Party(name='Pam')
    >>> _ = party.identifiers.new(code="Identifier", type=None)
    >>> _ = party.contact_mechanisms.new(type='other', value="mechanism")
    >>> party.allowed_for_sales = True
    >>> party.save()
    >>> address, = party.addresses
    >>> address.street = "St sample, 15"
    >>> address.city = "City"
    >>> address.save()

Create a party2::

    >>> party2 = Party(name='Pam 1')
    >>> _ = party2.identifiers.new(code="Identifier2", type=None)
    >>> _ = party2.contact_mechanisms.new(type='other', value="mechanism")
    >>> party2.allowed_for_sales = True
    >>> party2.save()
    >>> address2, = party2.addresses
    >>> address2.street = "St sample 2, 155555"
    >>> address2.city = "City 2"
    >>> address2.save()

Create employee::

    >>> Employee = Model.get('company.employee')
    >>> employee = Employee()
    >>> eparty = Party(name='Employee')
    >>> eparty.save()
    >>> employee.party = eparty
    >>> employee.company = company
    >>> employee.save()

Create account category::

    >>> ProductCategory = Model.get('product.category')
    >>> account_category = ProductCategory(name="Account Category")
    >>> account_category.accounting = True
    >>> account_category.account_expense = expense
    >>> account_category.account_revenue = revenue
    >>> account_category.customer_taxes.append(tax)
    >>> account_category.save()

Create products::

    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> product = Product()
    >>> template = ProductTemplate()
    >>> template.name = 'product'
    >>> template.default_uom = unit
    >>> template.type = 'goods'
    >>> template.salable = True
    >>> template.list_price = Decimal('10')
    >>> template.cost_price = Decimal('5')
    >>> template.cost_price_method = 'fixed'
    >>> template.account_category = account_category
    >>> template.save()
    >>> product, = template.products
    >>> product.save()
    >>> product1 = Product()
    >>> template1 = ProductTemplate()
    >>> template1.name = 'product 1'
    >>> template1.default_uom = unit
    >>> template1.type = 'goods'
    >>> template1.salable = True
    >>> template1.list_price = Decimal('10')
    >>> template1.cost_price = Decimal('5')
    >>> template1.cost_price_method = 'fixed'
    >>> template1.account_category = account_category
    >>> template1.save()
    >>> product1, = template1.products
    >>> product2 = Product()
    >>> template2 = ProductTemplate()
    >>> template2.name = 'product 2'
    >>> template2.default_uom = unit
    >>> template2.type = 'goods'
    >>> template2.salable = True
    >>> template2.list_price = Decimal('10')
    >>> template2.cost_price = Decimal('5')
    >>> template2.cost_price_method = 'fixed'
    >>> template2.account_category = account_category
    >>> template2.save()
    >>> product2, = template2.products

Create an Inventory ::

    >>> config.user = sale_user.id
    >>> storage, = Location.find([('code', '=', 'STO')])
    >>> inventory = Inventory()
    >>> inventory.location = storage
    >>> inventory_line = inventory.lines.new(product=product)
    >>> inventory_line.quantity = 100.0
    >>> inventory_line.expected_quantity = 0.0
    >>> inventory_line1 = inventory.lines.new(product=product1)
    >>> inventory_line1.quantity = 100.0
    >>> inventory_line1.expected_quantity = 0.0
    >>> inventory_line2 = inventory.lines.new(product=product2)
    >>> inventory_line2.quantity = 100.0
    >>> inventory_line2.expected_quantity = 0.0
    >>> inventory.click('confirm')
    >>> inventory.state
    'done'

Configuration Sales::

    >>> sale_config = SaleConfig(1)
    >>> sequence_type, = SequenceType.find([('name', '=', 'Sale')])
    >>> sale_sequence, = Sequence.find([('name', '=', 'Sale')])
    >>> sale_quotation_sequence, = Sequence.find([('name', '=', 'Sale Quotation')])
    >>> opportunity_sequence, = Sequence.find([('name', '=', 'Sale Opportunity')])
    >>> sale_config.sale_sequence = sale_sequence
    >>> sale_config.sale_quotation_sequence = sale_quotation_sequence
    >>> sale_config.approval_group = lead_times_approval
    >>> sale_config.save()

Create a Sale Order ::

    >>> config.user = sale_user.id
    >>> sale = Sale()
    >>> sale.not_from_presupuestario = True
    >>> sale.is_order = False
    >>> sale.in_force_period = datetime.timedelta(days=120)
    >>> sale.party = party
    >>> sale.invoice_address = party.addresses[0]
    >>> sale.shipment_address = party.addresses[0]
    >>> sale.currency = company.currency
    >>> sale.company = company
    >>> sale.save()
    >>> sale_line = sale.lines.new(product=product)
    >>> sale_line.quantity = 1
    >>> sale_line.unit = unit
    >>> sale_line.save()
    >>> sale_line1 = sale.lines.new(product=product1)
    >>> sale_line1.quantity = 1
    >>> sale_line1.unit = unit
    >>> sale_line1.save()
    >>> sale_line2 = sale.lines.new(product=product2)
    >>> sale_line2.quantity = 1
    >>> sale_line2.unit = unit
    >>> sale_line2.save()
    >>> sale.save()
    >>> sale.click('quote')
    >>> sale.state
    'quotation'
    >>> sale.lead_times_state
    'none'

Request lead  (state change to requested)::

    >>> sale.click('request_time_button')
    >>> sale.lead_times_state
    'requested'

Fill one sale line with lead times (sale validated_line_terms False because we have to fill 2 lines)::

    >>> sale_line.delivery_time = datetime.timedelta(days=1)
    >>> sale_line.validated_terms
    True
    >>> sale_line.manual_delivery_date == datetime.date(today.year, today.month, today.day + 1)
    True
    >>> sale_line.save()
    >>> sale.save()
    >>> sale.validated_line_terms
    False

Fill the other sale line with lead times(now validated_line_terms True)::

    >>> sale_line1.delivery_time = datetime.timedelta(days=3)
    >>> sale_line1.comment = "This is a sample comment"
    >>> sale_line1.validated_terms
    True
    >>> sale_line1.manual_delivery_date == datetime.date(today.year, today.month, today.day + 3)
    True
    >>> sale_line1.save()
    >>> sale_line2.delivery_time = datetime.timedelta(days=10)
    >>> sale_line2.comment = "This is a sample comment 2"
    >>> sale_line2.validated_terms
    True
    >>> sale_line2.save()
    >>> sale.save()
    >>> sale.validated_line_terms
    True

Test duplicate (defaults)::

    >>> sale2, = sale.duplicate()
    >>> sale2.lead_times_state
    'none'
    >>> sale2.validated_line_terms
    False

Request approval (state change to requested)::

    >>> sale.click('request_approval_button')
    >>> sale.lead_times_state
    'pending'

Case: Pending an approval request the terms are invalidated and approval_user tries to approve the request ::

    >>> sale.lines[0].delivery_time = None
    >>> sale.lines[0].validated_terms
    False
    >>> sale.lines[0].save()
    >>> sale.save()
    >>> sale.validated_line_terms
    False
    >>> config.user = approval_user.id
    >>> request = sale.approval_requests[0]
    >>> Request = Model.get('approval.request')
    >>> request.click('approve')
    >>> request.state
    'cancelled'
    >>> sale.lead_times_state
    'none'

Case: Request and approve new approval_request::

    >>> config.user = sale_user.id
    >>> sale.click('request_time_button')
    >>> sale.lines[0].delivery_time = datetime.timedelta(days=1)
    >>> sale.lines[0].validated_terms
    True
    >>> sale.lines[0].save()
    >>> sale.save()
    >>> sale.validated_line_terms
    True
    >>> sale.click('request_approval_button')
    >>> len(sale.approval_requests)
    2
    >>> config.user = approval_user.id
    >>> request = [request for request in sale.approval_requests if request.state == 'pending']
    >>> len(request)
    1
    >>> request = request[0]
    >>> request.click('approve')
    >>> request.state
    'approved'
    >>> sale.lead_times_state
    'approved'


Case: Invalidate the terms when approval_request approved (Raise on Wizard Create)::

    >>> sale.lines[0].delivery_time = None
    >>> sale.lines[0].validated_terms
    False
    >>> sale.lines[0].save()
    >>> sale.save()
    >>> sale.validated_line_terms
    False
    >>> create_order_wizard = Wizard('sale.create_order', [sale]) # doctest: +IGNORE_EXCEPTION_DETAIL
    Traceback (most recent call last):
        ...
    UserError: ...

Case: Fill again lead times, and create order::

    >>> sale.click('request_time_button')
    >>> sale.lines[0].delivery_time = datetime.timedelta(days=1)
    >>> sale.lines[0].validated_terms
    True
    >>> sale.lines[0].save()
    >>> sale.save()
    >>> sale.validated_line_terms
    True
    >>> sale.click('request_approval_button')
    >>> len(sale.approval_requests)
    3
    >>> config.user = approval_user.id
    >>> request = [request for request in sale.approval_requests if request.state == 'pending']
    >>> len(request)
    1
    >>> request = request[0]
    >>> request.click('approve')
    >>> request.state
    'approved'
    >>> sale.lead_times_state
    'approved'
    >>> create_order_wizard = Wizard('sale.create_order', [sale])
    >>> create_order_wizard.execute('create_')
    >>> sale.quotation_state
    'won'

Case: Sale with lead times. Create approval request and modify the date of the approval to check if expiration is working::

    >>> sale, = sale.duplicate()
    >>> sale.click('request_time_button')
    >>> for line in sale.lines:
    ...     line.delivery_time = datetime.timedelta(days=1)
    ...     line.save()
    >>> sale.save()
    >>> sale.click('quote')
    >>> sale.state
    'quotation'
    >>> sale.validated_line_terms
    True
    >>> sale.click('request_approval_button')
    >>> len(sale.approval_requests)
    1
    >>> request = sale.approval_requests[0]
    >>> request.request_date = datetime.datetime.now() - datetime.timedelta(days=20)
    >>> request.save()
    >>> config.user = approval_user.id
    >>> request.click('approve')
    >>> request.state
    'approved'
    >>> sale.lead_times_state
    'approved'
    >>> create_order_wizard = Wizard('sale.create_order', [sale]) # doctest: +IGNORE_EXCEPTION_DETAIL
    Traceback (most recent call last):
        ...
    UserError: ...

Case: Test wizard to fill lead times in sale lines:

    >>> sale, = sale.duplicate()
    >>> sale.click('request_time_button')
    >>> sale.lead_times_state
    'requested'
    >>> wizard = Wizard('sale.fill_lead_times')
    >>> wizard.form.lines.append(SaleLine(sale.lines[0].id))
    >>> wizard.form.lines.append(SaleLine(sale.lines[1].id))
    >>> wizard.form.delivery_time = datetime.timedelta(days=2)
    >>> wizard.form.comment = "This is a sample comment"
    >>> wizard.execute('apply_lead_times_lines')
    >>> sale.lines[0].delivery_time
    datetime.timedelta(days=2)
    >>> sale.lines[0].comment
    'This is a sample comment'
    >>> sale.lines[0].manual_delivery_date == datetime.date(today.year, today.month, today.day + 2)
    True
    >>> sale.lines[1].delivery_time
    datetime.timedelta(days=2)
    >>> sale.lines[1].comment
    'This is a sample comment'
    >>> sale.lines[1].manual_delivery_date == datetime.date(today.year, today.month, today.day + 2)
    True
