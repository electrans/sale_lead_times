# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool
from . import approval
from . import user
from . import sale


def register():
    Pool.register(
        approval.ApprovalRequest,
        user.User,
        sale.Configuration,
        sale.Sale,
        sale.SaleLine,
        sale.SaleLineLeadTimesStart,
        module='electrans_sale_lead_times', type_='model')
    Pool.register(
        sale.CreateOrder,
        sale.SaleLinesFillLeadTimes,
        module='electrans_sale_lead_times', type_='wizard')
