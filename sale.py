from trytond.pool import PoolMeta, Pool
from trytond.model import fields, ModelView
from trytond.pyson import Eval, Bool, Not, Or
from trytond.wizard import Wizard, Button, StateView, StateTransition
from datetime import datetime, timedelta
from trytond.transaction import Transaction
from trytond.exceptions import UserError, UserWarning
from trytond.i18n import gettext

# Default time to override Sale/SaleLine terms (days)
DEFAULT_SALE_LEAD_TIME = 15
# Clause for the fields/buttons to be hidden except in quotation
VISIBLE_IN_QUOTATION_SALE = Or(
    Not(Bool(Eval('not_from_presupuestario'))),
    Bool(Eval('is_order'))
)
# Depends for the fields/buttons to be hidden except in quotation
DEPENDS_4INVISBLE = ['not_from_presupuestario', 'is_order']
# Sale Line Clause for the fields/buttons to be hidden except in quotation
VISIBLE_LINE_IN_QUOTATION_SALE = Or(Or(
    Bool(Eval('_parent_sale', {}).get('is_order')),
    Not(Bool(Eval('_parent_sale', {}).get('not_from_presupuestario')))),
    Not(Bool(Eval('type', '') == 'line'))
)


class Configuration(metaclass=PoolMeta):
    __name__ = 'sale.configuration'

    lead_times_terms = fields.TimeDelta('Lead Times Terms', required=True,
                    help='Number of days to calculate lead times')
    approval_group = fields.Many2One('approval.group',
                                     'Lead Times - Approval Group',
                 help='Group that can approve the sale lead times')
    users_to_notify = fields.One2Many('res.user', 'notify_lead_times',
                    'Users to notify (Email - Request line terms)',
                    help='Users to notify when need to fill terms ')

    @staticmethod
    def default_lead_times_terms():
        """ Default value for lead times terms (in seconds)"""
        return DEFAULT_SALE_LEAD_TIME * 24 * 60 * 60


class SaleLine(metaclass=PoolMeta):
    __name__ = 'sale.line'

    delivery_time = fields.TimeDelta('Delivery Time', states={
        'invisible': VISIBLE_LINE_IN_QUOTATION_SALE,
        'readonly': Bool(Eval('_parent_sale', {}).get(
            'orders'))},
        help='Delivery time for this line. Example: 1 day, 2 hours 3 minutes')
    development_time = fields.TimeDelta('Development Timeframe', states={
        'invisible': VISIBLE_LINE_IN_QUOTATION_SALE,
        'readonly': Bool(Eval('_parent_sale', {}).get(
            'orders'))},
        help='Development time for this line. Example: 1 day, 2 hours 3 minutes')
    validated_terms = fields.Function(
        fields.Boolean('Validated Terms', states={
            'invisible': VISIBLE_LINE_IN_QUOTATION_SALE},
                       help='Validates if terms are filled'),
        'on_change_with_validated_terms', searcher='search_validated_terms')
    comment = fields.Text('Comment (Lead Times)', states={
        'invisible': VISIBLE_LINE_IN_QUOTATION_SALE,
        'readonly': Eval('sale_state') != 'draft'},
                          depends=['sale_state'],
                          help='Comment for this line (LEAD TIMES)')

    @classmethod
    def copy(cls, lines, default=None):
        if default is None:
            default = {}
        else:
            default = default.copy()
        default.setdefault('delivery_time', None)
        default.setdefault('development_time', None)
        default.setdefault('comment', None)
        return super(SaleLine, cls).copy(lines, default=default)

    @staticmethod
    def default_development_time():
        return None

    @staticmethod
    def default_validated_terms():
        return False

    @staticmethod
    def default_comment():
        return None

    @fields.depends('product')
    def on_change_product(self):
        super(SaleLine, self).on_change_product()
        self.reset_terms()

    @fields.depends('quantity')
    def on_change_quantity(self):
        super(SaleLine, self).on_change_quantity()
        self.reset_terms()

    @fields.depends('delivery_time', 'development_time')
    def on_change_delivery_time(self):
        self.compute_dates()

    @fields.depends('development_time', 'delivery_time')
    def on_change_development_time(self):
        self.compute_dates()

    @fields.depends('product', 'development_time', 'delivery_time')
    def on_change_with_validated_terms(self, name=None):
        # Check if product is empty to check if development_time have to set
        if not self.product:
            if self.development_time:
                return True
            else:
                return False
        else:
            # In other cases, check if delivery_term is set
            if self.delivery_time:
                return True
            else:
                return False

    @classmethod
    def search_validated_terms(cls, name, clause):
        if (clause[2] and clause[1] == '=') or (
                clause[1] in ['!', '!='] and not clause[2]):
            return [['OR', [('product', '=', None),
                     ('development_time', '!=', None)],
                    [('product', '!=', None),
                     ('delivery_time', '!=', None)]
                     ]]
        else:
            return [['OR', [('product', '=', None),
                     ('development_time', '=', None)],
                    [('product', '!=', None),
                     ('delivery_time', '=', None)]
                     ]]

    def reset_terms(self):
        """ Reset terms when 'condition' is changed """
        self.delivery_time = None
        self.development_time = None
        self.manual_delivery_date = None
        self.validated_terms = False

    def compute_dates(self):
        """
        Compute dates from delivery_time and development_time and set today
        validates_terms_date if conditions are met
        """
        pool = Pool()
        Date = pool.get('ir.date')
        WorkCalendar = pool.get('work.calendar')
        if self.delivery_time and self.development_time:
            self.manual_delivery_date = Date.today() + self.delivery_time + \
                                        self.development_time
        elif self.delivery_time and not self.development_time:
            self.manual_delivery_date = Date.today() + self.delivery_time
        elif not self.delivery_time and self.development_time:
            self.manual_delivery_date = Date.today() + self.development_time
        else:
            self.manual_delivery_date = None
        # Check if manual_delivery_date falls on holiday, if so, add the
        # corresponding days to manual_delivery_date til' the next business day
        # Get the calendar year of the same year of the manual_delivery_date
        if self.manual_delivery_date:
            work_calendar = WorkCalendar.search(
                [('year', '=', self.manual_delivery_date.year)])
            if work_calendar:
                # Get rest days defined in the calendar
                rest_days = work_calendar[0].rest_days
                # Get holidays dates of the same year as manual_delivery_date
                holidays = work_calendar[0].get_holidays()
                # Check if manual_delivery_date exists in
                # the off days defined in the current calendar
                # Make a loop to find the immediate next business date after
                # manual_delivery_date if it falls on rest_days or holidays
                while ((str(self.manual_delivery_date.isoweekday()) in
                        rest_days) or (self.manual_delivery_date in holidays)):
                    ant_year = self.manual_delivery_date.year
                    self.manual_delivery_date += timedelta(days=1)
                    # get the right calendar always. Examples:
                    # manual_delivery_date.year = 2023 and when you add 1
                    # day to it, manual_delivery_date.year = 2024
                    if self.manual_delivery_date.year != ant_year:
                        work_calendar = WorkCalendar.search(
                            [('year', '=', self.manual_delivery_date.year)])
                        holidays = (work_calendar[0].get_holidays()
                                    if work_calendar else [])


class Sale(metaclass=PoolMeta):
    __name__ = 'sale.sale'

    validated_line_terms = fields.Function(
        fields.Boolean('Validated Line Terms',
                       states={'invisible': VISIBLE_IN_QUOTATION_SALE},
                       depends=DEPENDS_4INVISBLE,
                       ), 'get_validated_line_terms')
    approval_group = fields.Function(
        fields.Many2One('approval.group', 'Approval Group'),
        'get_approval_group')
    approval_requests = fields.One2Many('approval.request', 'document',
                        'Approval Requests', readonly=True,
                        states={
                            'invisible': VISIBLE_IN_QUOTATION_SALE},
                        depends=DEPENDS_4INVISBLE,
                        )
    lead_times_state = fields.Selection([
        ('none', ''),
        ('requested', 'Requested'),
        ('pending', 'Pending'),
        ('approved', 'Approved'),
    ], 'Lead Times State', readonly=True, required=True,
        states={'invisible': VISIBLE_IN_QUOTATION_SALE},
        depends=DEPENDS_4INVISBLE,
        help='State of the lead times approval: \n- None: No lead times \n- Requested: Requested to fill line times \n-'
             ' Pending: Lead times approval pending \n- Approved: Lead times approval approved')

    @classmethod
    def __setup__(cls):
        super(Sale, cls).__setup__()
        cls._buttons.update({
            'request_time_button': {
                'invisible': Or(
                    Bool(Eval('validated_line_terms')),
                    VISIBLE_IN_QUOTATION_SALE
                ),
                'icon': 'request-time',
                'depends': ['validated_line_terms', 'not_from_presupuestario',
                            'is_order'],
            },
            'request_approval_button': {
                'invisible':
                    Bool(~Eval('validated_line_terms')) |
                    Bool(Eval('lead_times_state') == 'none') |
                    VISIBLE_IN_QUOTATION_SALE | Bool(Eval('orders')),
                'icon': 'tryton-forward',
                'depends': ['not_from_presupuestario', 'validated_line_terms',
                            'is_order', 'orders'],
            },
            'fill_lead_times_wizard_button': {
                'invisible':
                    Bool(Eval('validated_line_terms')) |
                    Bool(Eval('lead_times_state') == 'none') |
                    VISIBLE_IN_QUOTATION_SALE,
                'depends': ['not_from_presupuestario', 'validated_line_terms',
                            'is_order'],
            },
        })

    @classmethod
    def copy(cls, sales, default=None):
        if default is None:
            default = {}
        else:
            default = default.copy()
        default.setdefault('lead_times_state', 'none')
        default.setdefault('approval_requests', None)
        return super(Sale, cls).copy(sales, default=default)

    @staticmethod
    def default_lead_times_state():
        return 'none'

    def search_lines_not_validated_terms(self):
        SaleLine = Pool().get('sale.line')
        line_not_validated_terms = SaleLine.search([
            ('validated_terms', '=', False),
            ('type', '=', 'line'),
            ('sale', '=', self.id)
        ], limit=1)
        return line_not_validated_terms

    def get_validated_line_terms(self, name):
        """
        Check if all lines have validated terms and return True if in time
        """
        Config = Pool().get('sale.configuration')
        config = Config(1)
        decision_date = None
        # check line terms
        line_not_validated_terms = self.search_lines_not_validated_terms()
        if line_not_validated_terms:
            return False
        # if approved aproval request check date
        if self.lead_times_state == 'approved':
            for request in self.approval_requests:
                if request.state == 'approved':
                    if not decision_date:
                        decision_date = request.decision_date
                    elif request.decision_date > decision_date:
                        decision_date = request.decision_date
            if decision_date + config.lead_times_terms <= datetime.now():
                return False
        return True

    def get_approval_request(self, name=None, group=None):
        pool = Pool()
        Request = pool.get('approval.request')
        return Request(
            document=self,
            group=self.approval_group,
            request_date=datetime.now())

    def get_approval_state(self):
        '''
        Return the approval state for the sale (depends of request state)
        '''
        requests = self.approval_requests
        if requests and requests[-1].state == 'rejected':
            return 'pending'
        elif requests and requests[-1].state != 'cancelled':
            return requests[-1].state
        else:
            return 'none'

    def get_approval_group(self, name=None):
        '''
        Return the approval group from the configuration sale.
        '''
        Config = Pool().get('sale.configuration')
        config = Config(1)
        return config.approval_group.id if config.approval_group else None

    @classmethod
    def set_approval_state(cls, sales):
        '''
        Set the approval state.
        '''
        to_write = []
        for sale in sales:
            state = sale.get_approval_state()
            if sale.lead_times_state != state:
                to_write.extend(([sale], {
                    'lead_times_state': state,
                }))
        if to_write:
            cls.write(*to_write)

    @classmethod
    @ModelView.button
    def request_time_button(cls, sales):
        to_save = []
        pool = Pool()
        ApprovalRequest = pool.get('approval.request')
        Warning = pool.get('res.user.warning')
        for sale in sales:
            if sale.lead_times_state != 'requested':
                #  if have approval request cancel it
                if sale.approval_requests:
                    for request in sale.approval_requests:
                        if request.state in ['pending', 'approved']:
                            key = 'sale_%s' % sale.id
                            if Warning.check(key):
                                raise UserWarning(key, gettext(
                                    'electrans_sale_lead_times.request_already_sent',
                                    request=request.rec_name))
                            ApprovalRequest.cancel([request])
                            break
                sale.lead_times_state = 'requested'
                to_save.append(sale)
        if to_save:
            cls.save(to_save)

    @classmethod
    @ModelView.button_action(
        'electrans_sale_lead_times.sale_line_fill_lead_times_wizard')
    def fill_lead_times_wizard_button(cls, sales):
        pass

    @classmethod
    @ModelView.button
    def request_approval_button(cls, sales):
        """
        Create an approval request for the sale if exist raise an error
        """
        to_create = []
        to_save = []
        Request = Pool().get('approval.request')
        for sale in sales:
            """
            # Check if terms are validated before requesting an approval
            if not sale.validated_line_terms:
                raise UserError(gettext(
                    'electrans_sale_lead_times.no_validated_line_terms'))
            """
            request = sale.get_approval_request()
            if sale.lead_times_state == 'requested':
                # When sale line terms filled and validated_line_terms
                if request:
                    sale.lead_times_state = 'pending'
                    # Request(request)
                    to_save.append(sale)
                    to_create.append(request._save_values)
            elif sale.lead_times_state == 'pending':
                # Request already exists
                for request in sale.approval_requests:
                    if request.state == 'pending':
                        raise UserError(gettext(
                            'electrans_sale_lead_times.request_pending'))
                if request:
                    # If dont have any pending request create a new one
                    # This is the case when some request is rejected
                    new_request = Request(
                        document=sale,
                        group=sale.approval_group,
                        request_date=datetime.now())
                    to_create.append(new_request._save_values)
            elif sale.lead_times_state == 'approved':
                requests = Request.search([
                    ('document.id', 'in', [p.id for p in sales],
                        'sale.sale'),
                    ('state', 'in', ('pending', 'approved')),
                    ])
                if requests:
                    Request.cancel(requests)
                new_request = Request(
                    document=sale,
                    group=sale.approval_group,
                    request_date=datetime.now())
                to_create.append(new_request._save_values)
        if to_create:
            Request.create(to_create)
        if to_save:
            cls.save(to_save)

    def in_time(self):
        """
        Check if the sale is in time
        """
        Config = Pool().get('sale.configuration')
        config = Config(1)
        for request in self.approval_requests:
            if request.state == 'approved':
                if not request.request_date:
                    return False
                if request.request_date + config.lead_times_terms <= datetime.now():
                    return False
        return True


class CreateOrder(metaclass=PoolMeta):
    "Create Order"
    __name__ = 'sale.create_order'

    def transition_start(self):
        """
           Override Wizard to check if Lead times is validated to continue
        """
        context = Transaction().context
        pool = Pool()
        Sale = pool.get('sale.sale')
        Warning = pool.get('res.user.warning')
        sale = Sale(context['active_id'])
        res = super(CreateOrder, self).transition_start()
        # Check if the lead times are validated or if the sale origin is a
        # repair because this kind of sales does not need lead times
        if not (res and (
                sale.lead_times_state == 'approved' and
                sale.validated_line_terms and sale.in_time())
                or (sale.origin and sale.origin.__name__ == 'repair.package')):
            key = 'sale_%s' % sale.id
            if Warning.check(key):
                raise UserWarning(key, gettext(
                    'electrans_sale_lead_times.lead_times_not_validated'))
        return res

    def do_create_(self, action):
        pool = Pool()
        Sale = pool.get('sale.sale')
        quotation = Sale(self.record.id)
        for line in quotation.lines:
            # update delivery dates when sale order is created
            if line.delivery_time or line.development_time:
                line.compute_dates()
                line.save()
        return super(CreateOrder, self).do_create_(action)
    

class SaleLineLeadTimesStart(ModelView):
    'Sale Lines Fill Lead Times Start'
    __name__ = 'sale.line.lead.times.start'

    lines = fields.One2Many(
        'sale.line', None, "Lines",
        domain=[
            ('sale', '=', Eval('context', {}).get('active_id', -1)),
            ('type', '=', 'line'),
        ])
    delivery_time = fields.TimeDelta('Delivery Time',
                 help='Delivery time for this line. Example: 1 day, 2 hours 3 minutes')
    development_time = fields.TimeDelta('Development Timeframe',
                    help='Development time for this line. Example: 1 day, 2 hours 3 minutes')
    comment = fields.Text('Comment (Lead Times)',
          help='Comment for lines (LEAD TIMES)')


class SaleLinesFillLeadTimes(Wizard):
    'Sale Lines Discount'
    __name__ = 'sale.fill_lead_times'

    start = StateView('sale.line.lead.times.start',
                      'electrans_sale_lead_times.sale_line_lead_times_view_form',
                      [
                          Button('Cancel', 'end', 'tryton-cancel'),
                          Button('Apply', 'apply_lead_times_lines',
                                 'tryton-ok', default=True),
                      ])
    apply_lead_times_lines = StateTransition()

    def transition_apply_lead_times_lines(self):
        pool = Pool()
        SaleLine = pool.get('sale.line')
        for line in self.start.lines:
            if self.start.delivery_time:
                line.delivery_time = self.start.delivery_time
                line.on_change_delivery_time()
            if self.start.development_time:
                line.development_time = self.start.development_time
                line.on_change_development_time()
            if self.start.comment:
                if line.comment:
                    line.comment += '\n' + self.start.comment
                else:
                    line.comment = self.start.comment
            line.on_change_with_validated_terms()
        SaleLine.save(self.start.lines)
        return 'end'
